package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production

	UserServiceHost string
	UserServicePort int

	OrderServiceHost string
	OrderServicePort int

	SmsServiceHost string
	SmsServicePort int

	CatalogServiceHost string
	CatalogServicePort int

	ContentServiceHost string
	ContentServicePort int

	SettingsServiceHost string
	SettingsServicePort int

	ExcelServiceHost string
	ExcelServicePort int

	LogLevel string
	HTTPPort string

	MinioEndpoint       string
	MinioAccessKeyID    string
	MinioSecretAccesKey string

	SMTPHost         string
	SMTPHostPassword string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "prod"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":1235"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(getOrReturnDefault("USER_SERVICE_PORT", 7001))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
