package api

import (
	"net/http"

	//"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	// swaggerFiles "github.com/swaggo/files"
	// ginSwagger "github.com/swaggo/gin-swagger"

	// _ "bitbucket.org/alien_soft/gz_api_gateway/api/docs" //for swagger
	v1 "gitlab.com/g5567/api_getaway/api/handlers/v1"
	"gitlab.com/g5567/api_getaway/config"
	"gitlab.com/g5567/api_getaway/pkg/grpc_client"

	//"bitbucket.org/alien_soft/gz_api_gateway/pkg/http/middleware"
	"gitlab.com/g5567/api_getaway/pkg/logger"
)

//Config ...
type Config struct {
	Logger     logger.Logger
	GrpcClient *grpc_client.GrpcClient
	Cfg        config.Config
}

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(cnf Config) *gin.Engine {
	r := gin.New()

	r.Static("/images", "./static/images")

	r.Use(gin.Logger())

	r.Use(gin.Recovery())

	//r.Use(middleware.NewAuthorizer(cnf.CasbinEnforcer))

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = append(config.AllowHeaders, "*")
	// config.AllowHeaders = append(config.AllowHeaders, "Authorization")
	// config.AllowHeaders = append(config.AllowHeaders, "Accept")
	// config.AllowHeaders = append(config.AllowHeaders, "Content-Type")
	// config.AllowHeaders = append(config.AllowHeaders, "image/jpeg")
	// config.AllowHeaders = append(config.AllowHeaders, "image/png")
	//config.AllowMethods = append(config.AllowMethods, "OPTIONS")
	//config.AllowOrigins = append(config.AllowOrigins, "https://goodzone.vercel.app")

	r.Use(cors.New(config))

	//r.Use(func(context *gin.Context) {
	//context.Header("Access-Control-Allow-Origin", "*")
	//context.Writer.Header().Set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS")
	//context.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
	//context.Header("Access-Control-Allow-Credentials", "true")
	//})

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:     cnf.Logger,
		GrpcClient: cnf.GrpcClient,
		Cfg:        cnf.Cfg,
	})

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "Api gateway"})
	})

	//Product Service
	r.POST("/v1/product", handlerV1.CreateProduct)
	r.GET("/v1/product", handlerV1.FindProducts)
	r.POST("/v1/product/filter", handlerV1.FilterProducts)
	r.POST("/v1/v2/product/filter", handlerV1.FilterProductsVersion2)
	r.GET("/v1/product/:product_id", handlerV1.GetProduct)
	r.GET("/v1/product/:product_id/shops", handlerV1.GetProductShops)
	r.PUT("/v1/product/:product_id", handlerV1.UpdateProduct)
	r.PUT("/v1/product/:product_id/update-price", handlerV1.UpdateProductPrice)
	r.PUT("/v1/product/:product_id/update-property", handlerV1.UpdateProductProperty)
	r.DELETE("/v1/product/:product_id", handlerV1.DeleteProduct)
	
	//Upload File
	r.POST("/v1/upload", handlerV1.ImageUpload)

	// url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
