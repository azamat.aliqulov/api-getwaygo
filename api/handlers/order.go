package v1

import (
	"context"
	"encoding/json"
	"fmt"
	pbu "gitlab.com/g5567/api_getaway/us_protos/user_service"
	"net/http"

	// "gitlab.com/g5567/api_getaway/api/helpers"
	"gitlab.com/g5567/api_getaway/api/models"
	"gitlab.com/g5567/api_getaway/pkg/logger"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
)

// @Router /v1/order [post]
// @Summary Order Create
// @Description API for creating an order
// @Tags order
// @Accept  json
// @Produce  json
// @Param customer body models.CreateOrderModel true "customer"
// @Success 200 {object} models.OrderModel
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) CreateOrder(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		createOrder models.CreateOrderModelFormData
		orderItems  []*pbo.OrderItem
	)
	jspbMarshal.OrigName = true

	err := c.ShouldBind(&createOrder)
	if err != nil {
		h.log.Error("error while binding request", logger.Error(err), logger.Any("c", c))
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	err = json.Unmarshal([]byte(createOrder.Items), &orderItems)
	if err != nil {
		h.log.Error("error while unmarshalling items field", logger.Error(err), logger.Any("items", createOrder.Items))
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	// Checking products whether they are exist in store or not
	// customer, err := h.grpcClient.CustomerService().Get(context.Background(), &pbu.GetCustomerRequest{
	// 	Id: createOrder.CustomerID,
	// })

	// if handleGrpcErrWithMessage(c, h.log, err, "Error while getting customer") {
	// 	return
	// }
	// for i := 0; i < len(orderItems); i++ {
	// 	res, err := h.grpcClient.ProductService().Get(context.Background(), &pbc.GetProductRequest{
	// 		Id: orderItems[i].ProductId,
	// 	})

	// 	if handleGrpcErrWithMessage(c, h.log, err, "Error while getting product") {
	// 		return
	// 	}
	// 	if customer.Customer.GetArea() == "Toshkent" || customer.Customer.GetArea() == "Ташкент" {
	// 		if !(res.Product.InStock.GetTashkentCity()) {
	// 			var message string = res.Product.GetName()

	// 			h.log.Error("Product not exist", logger.String("Not acceptable", message))
	// 			c.JSON(http.StatusNotAcceptable, models.ResponseError{
	// 				Error: models.InternalServerError{
	// 					Code:    ErrorCodeNotAcceptable,
	// 					Message: message,
	// 				},
	// 			})
	// 			return
	// 		}
	// 	} else {
	// 		if !(res.Product.InStock.GetSamarkand()) {
	// 			var message string = res.Product.GetName()

	// 			h.log.Error("Product not exist", logger.String("Not acceptable", message))
	// 			c.JSON(http.StatusNotAcceptable, models.ResponseError{
	// 				Error: models.InternalServerError{
	// 					Code:    ErrorCodeNotAcceptable,
	// 					Message: message,
	// 				},
	// 			})
	// 			return
	// 		}
	// 	}
	// }

	res, err := h.grpcClient.OrderService().Create(
		context.Background(),
		&pbo.CreateOrderRequest{
			CustomerId:     createOrder.CustomerID,
			CustomerName:   createOrder.CustomerName,
			Address:        createOrder.Address,
			Longlat:        createOrder.Longlat,
			Phone:          createOrder.Phone,
			DeliveryMethod: createOrder.DeliveryMethod,
			PaymentMethod:  createOrder.PaymentMethod,
			Note:           createOrder.Note,
			Items:          orderItems,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while creating order") {
		return
	}

	fmt.Println("order", res.Order)

	js, err := jspbMarshal.MarshalToString(res.Order)
	if handleInternalWithMessage(c, h.log, err, "Error while marshalling") {
		return
	}

	// tg send
	err = helpers.SendTgMessage(res.Order)
	if err != nil {
		h.log.Error("error while sending message to telegram", logger.Any("err", err))
		return
	}

	err = helpers.SendAmoCrm()
	if err != nil {
		h.log.Error("error while sending email ", logger.Any("err", err))
		return
	}
	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// @Router /v1/v2/order [post]
// @Summary Order Create
// @Description API for creating an order (rebuild for mobile app)
// @Tags order
// @Accept  json
// @Produce  json
// @Param customer body models.CreateOrderModel true "customer"
// @Success 200 {object} models.OrderModel
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) CreateOrderVersion2(c *gin.Context) {
	var (
		unmarshaller jsonpb.Unmarshaler
		jspbMarshal  jsonpb.Marshaler
		createOrder  pbo.CreateOrderRequest
		model        models.OrderModel
	)
	jspbMarshal.OrigName = true

	err := unmarshaller.Unmarshal(c.Request.Body, &createOrder)
	if err != nil {
		h.log.Error("error while unmarshalling request", logger.Error(err), logger.Any("c", c))
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}
	orderItems := createOrder.Items

	// Checking products whether they are exist in store or not
	for i := 0; i < len(orderItems); i++ {
		res, err := h.grpcClient.ProductService().Get(context.Background(), &pbc.GetProductRequest{
			Id: orderItems[i].ProductId,
		})

		if handleGrpcErrWithMessage(c, h.log, err, "Error while getting product") {
			return
		}
		if !(res.Product.InStock.GetSamarkand() || res.Product.InStock.GetTashkentCity()) {
			var message string = res.Product.GetName()

			h.log.Error("Product not exist", logger.String("Not acceptable", message))
			c.JSON(http.StatusNotAcceptable, models.ResponseError{
				Error: models.InternalServerError{
					Code:    ErrorCodeNotAcceptable,
					Message: message,
				},
			})
			return
		}
	}

	res, err := h.grpcClient.OrderService().Create(
		context.Background(),
		&createOrder,
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while creating order") {
		return
	}

	// fmt.Println("order", res.Order)
	h.log.Debug("order response from grpc", logger.Any("order", res.Order))

	js, err := jspbMarshal.MarshalToString(res.Order)
	if handleInternalWithMessage(c, h.log, err, "Error while marshalling") {
		return
	}

	err = json.Unmarshal([]byte(js), &model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorCodeInternal,
				Message: "error while parsing proto to struct",
			},
		})
		h.log.Error("error while parsing proto to struct", logger.Error(err))
		h.log.Error("error of response" + js)
		return
	}

	// tg send
	err = helpers.SendTgMessage(res.Order)
	if err != nil {
		h.log.Error("error while sending message to telegram", logger.Any("err", err))
		//return
	}

	c.JSON(http.StatusOK, model)
}
